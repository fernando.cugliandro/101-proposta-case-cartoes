package com.fercugliandro.proposta.casecartoes.pagamento.service.impl;

import com.fercugliandro.proposta.casecartoes.cartoes.model.Cartao;
import com.fercugliandro.proposta.casecartoes.cartoes.repository.CartoesRepository;
import com.fercugliandro.proposta.casecartoes.pagamento.model.Pagamento;
import com.fercugliandro.proposta.casecartoes.pagamento.repository.PagamentoRepository;
import com.fercugliandro.proposta.casecartoes.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoServiceImpl implements PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartoesRepository cartoesRepository;

    @Override
    public Pagamento save(Pagamento pagamento) throws Exception{

        isValidCard(pagamento.getIdCartao());

        return pagamentoRepository.save(pagamento);
    }

    @Override
    public List<Pagamento> extrato(long idcartao) {
        return pagamentoRepository.findByIdCartao(idcartao);
    }

    private void isValidCard(long idCartao) throws Exception {
        Cartao card = cartoesRepository.findById(idCartao).get();
        if (card == null) {
            throw new Exception("Cartao Inexistente");
        }

        if (!card.isAtivo()) {
            throw new Exception("Cartao Inativo!");
        }
    }
}
