package com.fercugliandro.proposta.casecartoes.pagamento.model.dto;

import com.fercugliandro.proposta.casecartoes.pagamento.model.Pagamento;
import com.fercugliandro.proposta.casecartoes.pagamento.model.dto.request.CreatePagamentoRequest;
import com.fercugliandro.proposta.casecartoes.pagamento.model.dto.response.CreatePagamentoResponse;
import com.fercugliandro.proposta.casecartoes.pagamento.model.dto.response.DetailPagamentoResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {

    public Pagamento toPagamento(CreatePagamentoRequest request) {
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(request.getDescricao());
        pagamento.setIdCartao(request.getCartao_id());
        pagamento.setValor(request.getValor());

        return pagamento;
    }

    public CreatePagamentoResponse toCreatePagamentoResponse(Pagamento pagamento) {
        CreatePagamentoResponse response = new CreatePagamentoResponse();
        response.setDescricao(pagamento.getDescricao());
        response.setId(pagamento.getIdCompra());
        response.setValor(pagamento.getValor());
        response.setCartao_id(pagamento.getIdCartao());

        return response;
    }

    public List<DetailPagamentoResponse> toSummaryPagamentos(List<Pagamento> pagamentos) {
        List<DetailPagamentoResponse> response = new ArrayList<>();
        DetailPagamentoResponse detailResponse = null;

        for (Pagamento pagamento : pagamentos) {
            detailResponse = new DetailPagamentoResponse();
            detailResponse.setCartao_id(pagamento.getIdCartao());
            detailResponse.setId(pagamento.getIdCompra());
            detailResponse.setDescricao(pagamento.getDescricao());
            detailResponse.setValor(pagamento.getValor());

            response.add(detailResponse);
        }

        return response;
    }

}

