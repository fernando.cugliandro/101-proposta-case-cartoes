package com.fercugliandro.proposta.casecartoes.cartoes.model;

import com.fercugliandro.proposta.casecartoes.cartoes.model.dto.request.CreateCartaoRequest;
import com.fercugliandro.proposta.casecartoes.cartoes.model.dto.response.CartaoDetailsResponse;
import com.fercugliandro.proposta.casecartoes.cartoes.model.dto.response.ChangeStatusCartaoResponse;
import com.fercugliandro.proposta.casecartoes.cartoes.model.dto.response.CreateCartaoResponse;
import com.fercugliandro.proposta.casecartoes.cliente.model.Cliente;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao(CreateCartaoRequest request) {
        Cartao cartao = new Cartao();
        cartao.setCliente(new Cliente());
        cartao.getCliente().setId(request.getClienteId());
        cartao.setNumeroCartao(request.getNumero());

        return cartao;
    }

    public CreateCartaoResponse toCreateCartaoResponse(Cartao cartao) {
        CreateCartaoResponse response = new CreateCartaoResponse();
        response.setId(cartao.getIdCartao());
        response.setAtivo(cartao.isAtivo());
        response.setClienteId(cartao.getCliente().getId());
        response.setNumero(cartao.getNumeroCartao());

        return response;
    }

    public ChangeStatusCartaoResponse toChangeStausCartaoResponse(Cartao cartao) {
        ChangeStatusCartaoResponse response = new ChangeStatusCartaoResponse();
        response.setId(cartao.getIdCartao());
        response.setAtivo(cartao.isAtivo());
        response.setClienteId(cartao.getCliente().getId());
        response.setNumero(cartao.getNumeroCartao());

        return response;
    }

    public CartaoDetailsResponse toCartaoDetailsResponse(Cartao cartao) {
        CartaoDetailsResponse cartaoDetailsResponse = new CartaoDetailsResponse();

        cartaoDetailsResponse.setId(cartao.getIdCartao());
        cartaoDetailsResponse.setNumero(cartao.getNumeroCartao());
        cartaoDetailsResponse.setClienteId(cartao.getCliente().getId());
        cartaoDetailsResponse.setAtivo(cartao.isAtivo());

        return cartaoDetailsResponse;
    }


}
