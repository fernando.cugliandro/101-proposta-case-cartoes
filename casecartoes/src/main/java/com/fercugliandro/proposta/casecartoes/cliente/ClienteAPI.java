package com.fercugliandro.proposta.casecartoes.cliente;

import com.fercugliandro.proposta.casecartoes.cliente.model.dto.request.ClienteCreateRequest;
import com.fercugliandro.proposta.casecartoes.cliente.model.ClienteMapper;
import com.fercugliandro.proposta.casecartoes.cliente.model.Cliente;
import com.fercugliandro.proposta.casecartoes.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClienteAPI {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    @PostMapping(path = "/cliente", consumes = "application/json")
    public ResponseEntity<?> createCard(@RequestBody ClienteCreateRequest clienteCreateRequest) {
        try {

            Cliente cliente = clienteMapper.toCliente(clienteCreateRequest);
            cliente = clienteService.save(cliente);

            return new ResponseEntity<>(clienteMapper.toClienteCreateResponse(cliente), HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/cliente/{id}", consumes = "application/json")
    public ResponseEntity<?> createCard(@PathVariable long idCliente) {

        Cliente client = clienteService.findById(idCliente);
        return new ResponseEntity<>(clienteMapper.toClienteDetailResponse(client), HttpStatus.OK);

    }

}
