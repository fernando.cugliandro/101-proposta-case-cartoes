package com.fercugliandro.proposta.casecartoes.cliente.model.dto.request;

public class ClienteCreateRequest {

    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
