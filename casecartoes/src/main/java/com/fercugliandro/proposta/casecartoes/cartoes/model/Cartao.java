package com.fercugliandro.proposta.casecartoes.cartoes.model;

import com.fercugliandro.proposta.casecartoes.cliente.model.Cliente;

import javax.persistence.*;

@Entity(name = "cartao")
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long idCartao;

    @Column(name = "numerocartao")
    private String numeroCartao;

    @Column(name = "ativo")
    private boolean ativo;

    @ManyToOne
    private Cliente cliente;

    public long getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(long idCartao) {
        this.idCartao = idCartao;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
