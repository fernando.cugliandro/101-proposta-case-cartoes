package com.fercugliandro.proposta.casecartoes.cartoes.service;

import com.fercugliandro.proposta.casecartoes.cartoes.model.Cartao;

public interface CartoesService {

    Cartao save(Cartao cartao) throws Exception;

    Cartao activate(Cartao cartao);

    Cartao findByNumeroCartao(String numeroCartao);
}
