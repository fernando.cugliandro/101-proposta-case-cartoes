package com.fercugliandro.proposta.casecartoes.cliente.service;

import com.fercugliandro.proposta.casecartoes.cliente.model.Cliente;

public interface ClienteService {

    Cliente save(Cliente cliente);

    Cliente findById(long idCliente);
}
