package com.fercugliandro.proposta.casecartoes.pagamento;

import com.fercugliandro.proposta.casecartoes.pagamento.model.Pagamento;
import com.fercugliandro.proposta.casecartoes.pagamento.model.dto.PagamentoMapper;
import com.fercugliandro.proposta.casecartoes.pagamento.model.dto.request.CreatePagamentoRequest;
import com.fercugliandro.proposta.casecartoes.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoAPI {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    @PostMapping(value = "/pagamento", consumes = "application/json")
    public ResponseEntity<?> doPagamento(@RequestBody CreatePagamentoRequest request) {
        try {

            Pagamento pagamento = pagamentoMapper.toPagamento(request);
            pagamento = pagamentoService.save(pagamento);
            return new ResponseEntity<>(pagamentoMapper.toCreatePagamentoResponse(pagamento), HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/pagamentos/{idcartao}")
    public ResponseEntity<?> extrato(@PathVariable long idcartao) {
        try {

            List<Pagamento> payments = pagamentoService.extrato(idcartao);
            return new ResponseEntity<>(pagamentoMapper.toSummaryPagamentos(payments), HttpStatus.OK);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

}
