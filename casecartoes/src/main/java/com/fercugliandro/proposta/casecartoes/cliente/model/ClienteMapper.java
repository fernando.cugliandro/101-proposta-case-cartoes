package com.fercugliandro.proposta.casecartoes.cliente.model;

import com.fercugliandro.proposta.casecartoes.cliente.model.dto.request.ClienteCreateRequest;
import com.fercugliandro.proposta.casecartoes.cliente.model.dto.response.ClienteCreateResponse;
import com.fercugliandro.proposta.casecartoes.cliente.model.Cliente;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente toCliente(ClienteCreateRequest request) {
        Cliente cliente = new Cliente();
        cliente.setName(request.getName());

        return cliente;
    }

    public ClienteCreateResponse toClienteCreateResponse(Cliente cliente) {
        ClienteCreateResponse response = new ClienteCreateResponse();
        response.setId(cliente.getId());
        response.setName(cliente.getName());

        return response;
    }

    public ClienteCreateResponse toClienteDetailResponse(Cliente cliente) {
        ClienteCreateResponse response = new ClienteCreateResponse();
        response.setId(cliente.getId());
        response.setName(cliente.getName());

        return response;
    }

}
