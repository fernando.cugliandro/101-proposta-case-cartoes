package com.fercugliandro.proposta.casecartoes.cartoes;

import com.fercugliandro.proposta.casecartoes.cartoes.model.Cartao;
import com.fercugliandro.proposta.casecartoes.cartoes.model.CartaoMapper;
import com.fercugliandro.proposta.casecartoes.cartoes.model.dto.request.ChangeStatusCartaoRequest;
import com.fercugliandro.proposta.casecartoes.cartoes.model.dto.request.CreateCartaoRequest;
import com.fercugliandro.proposta.casecartoes.cartoes.service.CartoesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CartoesAPI {

    @Autowired
    private CartoesService cartoesService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping(path = "/cartao", consumes = "application/json")
    public ResponseEntity<?> createCard(@RequestBody CreateCartaoRequest request) {
        try {
            Cartao cartao = cartaoMapper.toCartao(request);
            cartao = cartoesService.save(cartao);
            return new ResponseEntity<>(cartaoMapper.toCreateCartaoResponse(cartao), HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping(path = "/cartao/{numero}", consumes = "application/json")
    public ResponseEntity<?> createCard(@PathVariable String numero, @RequestBody ChangeStatusCartaoRequest request) {
        try {
            Cartao cartao = new Cartao();
            cartao.setNumeroCartao(numero);
            cartao.setAtivo(request.isAtivo());

            Cartao savedCard = cartoesService.activate(cartao);

            return new ResponseEntity<>(cartaoMapper.toChangeStausCartaoResponse(savedCard), HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/cartao/{numero}")
    public ResponseEntity<?> createCard(@PathVariable String numero) {
        try {

            Cartao savedCard = cartoesService.findByNumeroCartao(numero);

            return new ResponseEntity<>(cartaoMapper.toChangeStausCartaoResponse(savedCard), HttpStatus.CREATED);
        } catch (Exception e) {
            String errorMessage;
            errorMessage = e.getMessage() + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

}
