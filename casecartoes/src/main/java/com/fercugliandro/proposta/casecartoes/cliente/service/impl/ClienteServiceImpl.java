package com.fercugliandro.proposta.casecartoes.cliente.service.impl;

import com.fercugliandro.proposta.casecartoes.cliente.exception.ClienteNotFoundException;
import com.fercugliandro.proposta.casecartoes.cliente.model.Cliente;
import com.fercugliandro.proposta.casecartoes.cliente.repository.ClienteRepository;
import com.fercugliandro.proposta.casecartoes.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public Cliente save(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    @Override
    public Cliente findById(long idCliente) {
        Optional<Cliente> client = clienteRepository.findById(idCliente);

        if (!client.isPresent()) {
            throw new ClienteNotFoundException();
        }

        return client.get();
    }
}
