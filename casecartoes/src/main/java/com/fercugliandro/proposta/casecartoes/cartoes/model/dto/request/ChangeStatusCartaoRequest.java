package com.fercugliandro.proposta.casecartoes.cartoes.model.dto.request;

public class ChangeStatusCartaoRequest {

    private boolean ativo;

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
