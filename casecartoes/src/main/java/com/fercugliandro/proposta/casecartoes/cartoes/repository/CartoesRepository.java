package com.fercugliandro.proposta.casecartoes.cartoes.repository;

import com.fercugliandro.proposta.casecartoes.cartoes.model.Cartao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartoesRepository extends JpaRepository<Cartao, Long> {

    Optional<Cartao> findByNumeroCartao(String numeroCartao);

}
