package com.fercugliandro.proposta.casecartoes.cartoes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cartão não econtrado")
public class CartaoNotFoundException extends RuntimeException {
}
