package com.fercugliandro.proposta.casecartoes.pagamento.service;

import com.fercugliandro.proposta.casecartoes.pagamento.model.Pagamento;

import java.util.List;

public interface PagamentoService {

    Pagamento save(Pagamento pagamento) throws Exception;

    List<Pagamento> extrato(long idcartao);
}
